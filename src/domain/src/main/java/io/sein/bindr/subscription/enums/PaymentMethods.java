package io.sein.bindr.subscription.enums;

public enum PaymentMethods {
    PAYPAL("paypal"),

    GPAY("gpay"),

    STRIPE("stripe");

    private String value;

    PaymentMethods(String value) {
      this.value = value;
    }
}

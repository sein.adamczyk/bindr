package io.sein.bindr.core.events;


public interface DomainEventsPublisher {
    void publish(DomainEvent domainEvent);
}

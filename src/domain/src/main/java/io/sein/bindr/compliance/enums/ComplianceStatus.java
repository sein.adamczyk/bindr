package io.sein.bindr.compliance.enums;

public enum ComplianceStatus {

    PENDING("pending"),

    REJECTED("rejected"),

    ACCEPTED("accepted");

    private String value;

    ComplianceStatus(String value) {
      this.value = value;
    }
}

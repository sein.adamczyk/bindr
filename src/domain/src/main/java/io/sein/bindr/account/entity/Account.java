package io.sein.bindr.account.entity;

import io.sein.bindr.account.Permissions;
import jakarta.validation.constraints.NotEmpty;
import lombok.*;

import javax.persistence.*;
import java.util.Collections;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Builder()
@Table(name = "accounts")
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty
    @Column(unique = true)
    private String email;

    @NotEmpty
    private String password;

    private String phoneNr;

    @OneToOne()
    private Role role;

    public boolean hasRole() {
        return role != null;
    }

    public List<Permissions> getAuthorities() {
        return hasRole()
                ? getRole().getPermissions()
                : Collections.emptyList();
    }
}

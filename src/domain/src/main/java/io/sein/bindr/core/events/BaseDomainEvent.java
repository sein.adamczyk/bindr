package io.sein.bindr.core.events;

import org.springframework.context.ApplicationEvent;

import java.time.Instant;
import java.util.UUID;


public abstract class BaseDomainEvent extends ApplicationEvent implements DomainEvent  {

    private final UUID id;
    private final Instant occurredAt;

    public BaseDomainEvent(Object source) {
        super(source);
        this.id = UUID.randomUUID();
        this.occurredAt = Instant.now();
    }

    @Override
    public UUID id() {
        return id;
    }

    @Override
    public Instant occurredAt() {
        return occurredAt;
    }
}

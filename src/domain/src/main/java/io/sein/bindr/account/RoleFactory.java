package io.sein.bindr.account;

import io.sein.bindr.account.entity.Role;
import org.springframework.stereotype.Component;

@Component
public interface RoleFactory<T> {
    public Role createRole(T parameters);
}

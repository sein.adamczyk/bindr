package io.sein.bindr.core.exceptions;

public class UnmetSpecificationException extends RuntimeException {
    public UnmetSpecificationException(String message) {
        super(message);
    }
}

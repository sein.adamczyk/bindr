package io.sein.bindr.core;

public interface DomainPolicy {
    public Boolean isAllowed(DomainCommand command);
}

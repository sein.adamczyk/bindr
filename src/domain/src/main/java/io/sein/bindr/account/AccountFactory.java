package io.sein.bindr.account;

import io.sein.bindr.account.entity.Account;
import org.springframework.stereotype.Component;

@Component
public interface AccountFactory<T> {
    public Account createAccount(T parameters);
}

package io.sein.bindr.subscription.enums;

public enum SubscriptionPeriod {

    MONTH("month"),

    YEAR("year"),

    LIFETIME("lifetime");

    private String value;

    SubscriptionPeriod(String value) {
      this.value = value;
    }

}

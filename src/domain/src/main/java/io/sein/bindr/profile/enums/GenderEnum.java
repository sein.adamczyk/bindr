package io.sein.bindr.profile.enums;

  public enum GenderEnum {
    MALE("male"),

    FEMALE("female"),

    TRANS("trans"),

    COUPLE("couple");

    private String value;

    GenderEnum(String value) {
      this.value = value;
    }
}

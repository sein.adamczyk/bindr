package io.sein.bindr.account.enums;

public enum IntegrationTypes {
    FACEBOOK("facebook"),

    GOOGLE("google");

    private String value;

    IntegrationTypes(String value) {
      this.value = value;
    }
}

package io.sein.bindr.subscription.enums;

public enum TransactionStatus {

    PENDING("pending"),

    ACCEPTED("accepted"),

    REJECTED("rejected");

    private String value;

    TransactionStatus(String value) {
      this.value = value;
    }
}

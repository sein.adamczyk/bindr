package io.sein.bindr.core.exceptions;

public class DomainNotFoundException extends RuntimeException {
    public DomainNotFoundException() {
        super();
    }

    public DomainNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
    public DomainNotFoundException(String message) {
        super(message);
    }
    public DomainNotFoundException(Throwable cause) {
        super(cause);
    }
}

package io.sein.bindr.subscription.values;


public class BillingInformation {
    public String firstName;
    public String lastName;
    public String country;
    public String city;
    public String postalCode;
    public String address;
}

package io.sein.bindr.core.events;

import java.time.Instant;
import java.util.UUID;


public interface DomainEvent {

    UUID id();

    Instant occurredAt();

    String type();
}

package io.sein.bindr.subscription.enums;

public enum SubscriptionPlanStatus {

    ACTIVE("active"),

    DISABLED("disabled");

    private String value;

    SubscriptionPlanStatus(String value) {
      this.value = value;
    }

}

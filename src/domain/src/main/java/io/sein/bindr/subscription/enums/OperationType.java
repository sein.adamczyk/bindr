package io.sein.bindr.subscription.enums;

public enum OperationType {
     CHARGE("charge"),

    PAYMENT("payment");

    private String value;

    OperationType(String value) {
      this.value = value;
    }
}

package io.sein.bindr.core;

import io.sein.bindr.core.exceptions.UnmetSpecificationException;
import org.springframework.stereotype.Component;

@Component
public interface DomainSpecification<T> {
    public void isSatisfiedBy(T model) throws UnmetSpecificationException;
}

package io.sein.bindr.account;

public enum Permissions {

    CAN_WRITE_ROLES(Value.CAN_WRITE_ROLES),
    CAN_READ_ROLES(Value.CAN_READ_ROLES),
    CAN_MANAGE_ROLES(Value.CAN_MANAGE_ROLES),
    CAN_READ_USERS(Value.CAN_READ_USERS),
    CAN_MANAGE_USERS(Value.CAN_MANAGE_USERS),
    CAN_READ_LIKES(Value.CAN_READ_LIKES);

    private String value;

    Permissions(String value) {
      this.value = value;
    }

    public String toString() {
       return this.value;
    }

    public static class Value {

        public static final String CAN_WRITE_ROLES = "CAN_WRITE_ROLES";
        public static final String CAN_READ_ROLES = "CAN_READ_ROLES";
        public static final String CAN_MANAGE_ROLES = "CAN_MANAGE_ROLES";
        public static final String CAN_READ_USERS = "CAN_READ_USERS";
        public static final String CAN_MANAGE_USERS = "CAN_MANAGE_USERS";
        public static final String CAN_READ_LIKES = "CAN_READ_LIKES";

    }
}

package io.sein.bindr.compliance.enums;

public enum ComplianceType {

    FAKE("fake"),

    INAPPROPRIATE("inappropriate"),

    UNDERAGE("underage"),

    DANGER("danger"),

    VIOLATION("violation"),

    CRIME("crime");

    private String value;

    ComplianceType(String value) {
      this.value = value;
    }
}

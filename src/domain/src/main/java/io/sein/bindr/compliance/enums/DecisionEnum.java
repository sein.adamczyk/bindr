package io.sein.bindr.compliance.enums;


public enum DecisionEnum {
    KICK("kick"),

    BAN("ban"),

    WARNING("warning"),

    IGNORE("ignore");

    private String value;

    DecisionEnum(String value) {
        this.value = value;
    }
}

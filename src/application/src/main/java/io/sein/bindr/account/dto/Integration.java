package io.sein.bindr.account.dto;

import io.sein.bindr.account.enums.IntegrationTypes;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

/**
 * Integration
 */
@Validated
@Data
public class Integration   {
  @JsonProperty("remoteId")
  private String remoteId = null;

  @JsonProperty("accountName")
  private String accountName = null;

  @JsonProperty("type")
  private IntegrationTypes type = null;
}


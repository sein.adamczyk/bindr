package io.sein.bindr.account.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.validation.annotation.Validated;
import io.sein.bindr.subscription.enums.TransactionStatus;

import java.math.BigDecimal;

/**
 * TransactionHistory
 */
@Validated


@Data
public class TransactionHistory   {
  @JsonProperty("amount")
  private BigDecimal amount = null;

  @JsonProperty("status")
  private TransactionStatus status = null;

  @JsonProperty("gateway")
  private String gateway = null;

  @JsonProperty("log")
  private String log = null;
}


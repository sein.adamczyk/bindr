package io.sein.bindr.account.dto.request;

import io.sein.bindr.account.Permissions;
import lombok.Data;

import java.util.List;

@Data
public class RoleDefinitionRequest {
    private String name;
    private String description;
    private List<Permissions> permissions;
}

package io.sein.bindr.account.dto.request;

import io.sein.bindr.core.validators.PasswordMatches;
import io.sein.bindr.core.validators.ValidEmail;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
@PasswordMatches
public class RegisterRequest {

  @NotNull
  @NotEmpty
  @ValidEmail
  String email;

  @NotNull
  @NotEmpty
  String password;

  @NotNull
  @NotEmpty
  String password2;
}

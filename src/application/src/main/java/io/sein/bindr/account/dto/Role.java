package io.sein.bindr.account.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.sein.bindr.account.Permissions;
import jakarta.validation.Valid;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

import java.util.List;

/**
 * Role
 */
@Validated


@Data
public class Role   {
  @JsonProperty("id")
  private String id = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("descriptions")
  private String descriptions = null;

  @JsonProperty("permissions")
  @Valid
  private List<Permissions> permissions = null;
}


package io.sein.bindr.account.service;


import io.sein.bindr.account.dto.request.LoginRequest;
import io.sein.bindr.account.dto.request.RegisterRequest;

// TODO: Move to application layer?
public interface AuthenticationService {
    public String register(RegisterRequest request);
    public String login(LoginRequest request);
}

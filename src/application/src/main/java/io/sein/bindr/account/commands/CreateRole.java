package io.sein.bindr.account.commands;

import io.sein.bindr.account.Permissions;
import io.sein.bindr.core.DomainCommand;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class CreateRole implements DomainCommand {

    private final String name;
    private final String description;
    private final List<Permissions> permissions;
}

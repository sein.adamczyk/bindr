package io.sein.bindr.account;

public interface RolesCommandHandler {
    public <T> void handle(T command);
}

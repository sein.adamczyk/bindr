package io.sein.bindr.account.commands;

import io.sein.bindr.account.Permissions;
import io.sein.bindr.core.DomainCommand;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class ModifyRole implements DomainCommand {

    private final Long roleId;
    private final String description;
    private final List<Permissions> permissions;
}

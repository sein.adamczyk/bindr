package io.sein.bindr.account.commands;

import io.sein.bindr.core.DomainCommand;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class UpdateAccount implements DomainCommand {

    private final Long accountId;
    private final String email;
    private final String phoneNr;
}

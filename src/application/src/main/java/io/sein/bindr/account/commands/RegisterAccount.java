package io.sein.bindr.account.commands;

import io.sein.bindr.core.DomainCommand;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class RegisterAccount implements DomainCommand {

    private final String email;
    private final String password;
}

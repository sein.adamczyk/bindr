package io.sein.bindr.account.commands;

import io.sein.bindr.core.DomainCommand;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class DeleteAccount implements DomainCommand {

    private final Long accountId;
}

package io.sein.bindr.account.dto.response;

import lombok.Data;
import org.springframework.validation.annotation.Validated;

/**
 * Account
 */
@Validated

@Data
public class AccountResponse {
  private String id;
  private String email;
  private String phone;
}


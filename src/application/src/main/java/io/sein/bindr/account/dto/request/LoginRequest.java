package io.sein.bindr.account.dto.request;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;


@Data
public class LoginRequest {

  @NotNull
  @NotEmpty
  String email;

  @NotNull
  @NotEmpty
  String password;
}

package io.sein.bindr.account.commands;

import io.sein.bindr.core.DomainCommand;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class RemoveRole implements DomainCommand {
    private final Long roleId;
}

package io.sein.bindr.account.dto.request;

import lombok.Data;
import org.springframework.validation.annotation.Validated;

/**
 * Account
 */
@Validated

@Data
public class AccountRequest {
  private String email;
  private String phoneNr;
}


package io.sein.bindr.account.dto.request;

import lombok.Data;

@Data
public class RoleRequest {
    String roleId;
}

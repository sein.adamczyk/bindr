package io.sein.bindr.chat.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

import java.time.OffsetDateTime;

/**
 * Message
 */
@Validated


@Data
public class Message   {
  @JsonProperty("author")
  private Author author = null;

  @JsonProperty("content")
  private String content = null;

  @JsonProperty("sentAt")
  private OffsetDateTime sentAt = null;

  public Message author(Author author) {
    this.author = author;
    return this;
  }
}


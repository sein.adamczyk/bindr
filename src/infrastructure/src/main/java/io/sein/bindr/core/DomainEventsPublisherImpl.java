package io.sein.bindr.core;

import io.sein.bindr.core.events.DomainEvent;
import io.sein.bindr.core.events.DomainEventsPublisher;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class DomainEventsPublisherImpl implements DomainEventsPublisher {

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @Override
    public void publish(DomainEvent domainEvent) {
        log.info("Publishing custom event. {}", domainEvent.type());
        applicationEventPublisher.publishEvent(domainEvent);
    }
}

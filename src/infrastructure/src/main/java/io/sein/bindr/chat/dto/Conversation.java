package io.sein.bindr.chat.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.time.OffsetDateTime;
import java.util.List;

/**
 * Conversation
 */
@Validated


@Data
public class Conversation   {
  @JsonProperty("id")
  private String id = null;

  @JsonProperty("participants")
  @Valid
  private List<Author> participants = null;

  @JsonProperty("createdAt")
  private OffsetDateTime createdAt = null;
}


package io.sein.bindr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BindrApplication {

	public static void main(String[] args) {
		SpringApplication.run(BindrApplication.class, args);
	}

}

package io.sein.bindr.profile.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

/**
 * Picture
 */
@Validated

@Data
public class Picture   {
  @JsonProperty("title")
  private String title = null;

  @JsonProperty("path")
  private String path = null;

  @JsonProperty("filename")
  private String filename = null;

  @JsonProperty("url")
  private String url = null;
}


package io.sein.bindr.compliance.dto;

import io.sein.bindr.compliance.enums.DecisionEnum;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ComplianceDecision   {
  @JsonProperty("reportId")
  private String reportId = null;

  /**
   * Gets or Sets decision
   */


  @JsonProperty("decision")
  private DecisionEnum decision = null;

  @JsonProperty("reasoning")
  private String reasoning = null;
}


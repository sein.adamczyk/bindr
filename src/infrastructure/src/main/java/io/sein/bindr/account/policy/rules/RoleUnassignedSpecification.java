package io.sein.bindr.account.policy.rules;

import io.sein.bindr.account.AccountRepository;
import io.sein.bindr.account.RoleRepository;
import io.sein.bindr.account.entity.Role;
import io.sein.bindr.core.DomainSpecification;
import io.sein.bindr.core.exceptions.UnmetSpecificationException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class RoleUnassignedSpecification implements DomainSpecification<Long> {

    private final RoleRepository roleRepository;
    private final AccountRepository accountRepository;

    @Override
    public void isSatisfiedBy(Long roleId) {
        Role role = roleRepository.getOne(roleId);
        if (!accountRepository.findAllByRole(role).isEmpty()) {
            throw new UnmetSpecificationException("Role is still assigned to a user");
        }
    }
}

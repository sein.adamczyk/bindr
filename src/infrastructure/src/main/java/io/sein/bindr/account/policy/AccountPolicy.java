package io.sein.bindr.account.policy;

import io.sein.bindr.account.commands.RegisterAccount;
import io.sein.bindr.account.policy.rules.AccountExistsSpecification;
import io.sein.bindr.account.policy.rules.PasswordComplexitySpecification;
import io.sein.bindr.core.DomainCommand;
import io.sein.bindr.core.DomainPolicy;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.stereotype.Component;


@Component
@RequiredArgsConstructor
public class AccountPolicy implements DomainPolicy {

    private final AccountExistsSpecification existsSpecification;
    private final PasswordComplexitySpecification passwordComplex;

    @Override
    public Boolean isAllowed(DomainCommand command) {
        throw new NotImplementedException("Policy not implemented");
    }

    public Boolean isAllowed(RegisterAccount command) {
        existsSpecification.isSatisfiedBy(command.getEmail());
        passwordComplex.isSatisfiedBy(command.getPassword());
        return true;
    }

}

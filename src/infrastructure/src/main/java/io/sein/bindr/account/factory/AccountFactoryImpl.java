package io.sein.bindr.account.factory;

import io.sein.bindr.account.AccountFactory;
import io.sein.bindr.account.commands.RegisterAccount;
import io.sein.bindr.account.entity.Account;
import io.sein.bindr.account.policy.AccountPolicy;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class AccountFactoryImpl implements AccountFactory<RegisterAccount> {

    private final AccountPolicy accountPolicy;
    private final PasswordEncoder passwordEncoder;

    @Override
    public Account createAccount(RegisterAccount parameters) {
        accountPolicy.isAllowed(parameters);

        return Account.builder()
                .email(parameters.getEmail())
                .password(passwordEncoder.encode(parameters.getPassword()))
                .build();
    }
}

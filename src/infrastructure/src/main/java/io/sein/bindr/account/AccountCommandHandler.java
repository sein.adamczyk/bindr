package io.sein.bindr.account;

import io.sein.bindr.account.commands.*;
import io.sein.bindr.account.entity.Account;
import io.sein.bindr.account.entity.Role;
import io.sein.bindr.account.policy.AccountPolicy;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class AccountCommandHandler {
    private final AccountRepository accountRepository;
    private final RoleRepository roleRepository;
    private final AccountPolicy accountPolicy;
    private final ModelMapper modelMapper = new ModelMapper();

    public void handle(RegisterAccount command) {

    }

    public void handle(AssignRole command) {
        accountPolicy.isAllowed(command);
        Account account = accountRepository.getOne(command.getAccountId());
        Role role = roleRepository.getOne(command.getRoleId());
        account.setRole(role);
        accountRepository.save(account);
    }

    public void handle(RevokeRole command) {
        accountPolicy.isAllowed(command);
        Account account = accountRepository.getOne(command.getAccountId());
        account.setRole(null);
        accountRepository.save(account);
    }

    public void handle(DeactivateAccount command) {

    }

    public void handle(DeleteAccount deleteAccount) {
    }

    public void handle(UpdateAccount updateAccount) {
    }
}

package io.sein.bindr.subscription.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.validation.annotation.Validated;
import io.sein.bindr.subscription.enums.OperationType;

import java.math.BigDecimal;

/**
 * Operation
 */
@Validated


@Data
public class Operation   {
  @JsonProperty("accountId")
  private String accountId = null;


  @JsonProperty("type")
  private OperationType type = null;

  @JsonProperty("referenceId")
  private String referenceId = null;

  @JsonProperty("amount")
  private BigDecimal amount = null;
}


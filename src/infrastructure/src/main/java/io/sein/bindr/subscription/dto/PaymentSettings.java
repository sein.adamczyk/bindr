package io.sein.bindr.subscription.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.validation.annotation.Validated;
import io.sein.bindr.subscription.enums.PaymentMethods;
import io.sein.bindr.subscription.values.BillingInformation;

/**
 * PaymentSettings
 */
@Validated


@Data
public class PaymentSettings   {
  @JsonProperty("method")
  private PaymentMethods method = null;

  @JsonProperty("billingInfo")
  private BillingInformation billingInfo = null;
}


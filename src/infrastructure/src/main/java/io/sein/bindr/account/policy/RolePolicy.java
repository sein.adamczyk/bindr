package io.sein.bindr.account.policy;

import io.sein.bindr.account.commands.CreateRole;
import io.sein.bindr.account.commands.ModifyRole;
import io.sein.bindr.account.commands.RemoveRole;
import io.sein.bindr.account.policy.rules.RoleExistsSpecification;
import io.sein.bindr.account.policy.rules.RoleNameUniqueSpecification;
import io.sein.bindr.account.policy.rules.RoleUnassignedSpecification;
import io.sein.bindr.core.DomainCommand;
import io.sein.bindr.core.DomainPolicy;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class RolePolicy implements DomainPolicy {

    private final RoleExistsSpecification roleExistsSpecification;
    private final RoleNameUniqueSpecification roleNameUniqueSpecification;
    private final RoleUnassignedSpecification roleUnassignedSpecification;

    @Override
    public Boolean isAllowed(DomainCommand command) {
        throw new NotImplementedException("Policy not implemented");
    }

    public Boolean isAllowed(CreateRole command) {
        roleNameUniqueSpecification.isSatisfiedBy(command.getName());
        return true;
    }

    public Boolean isAllowed(ModifyRole command) {
        roleExistsSpecification.isSatisfiedBy(command.getRoleId());

        return true;
    }

    public Boolean isAllowed(RemoveRole command) {
        roleExistsSpecification.isSatisfiedBy(command.getRoleId());
        roleUnassignedSpecification.isSatisfiedBy(command.getRoleId());
        return true;
    }
}

package io.sein.bindr.account.endpoint;

import io.sein.bindr.account.RoleCommandHandler;
import io.sein.bindr.account.RoleQueryHandler;
import io.sein.bindr.account.commands.CreateRole;
import io.sein.bindr.account.commands.ModifyRole;
import io.sein.bindr.account.commands.RemoveRole;
import io.sein.bindr.account.dto.request.RoleDefinitionRequest;
import io.sein.bindr.account.entity.Role;
import io.sein.bindr.account.queries.GetRoleQuery;
import io.sein.bindr.account.queries.ListRolesQuery;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1")
@SecurityRequirement(name = "bearerAuth")
public class RolesManagementEndpoint {

    private final RoleCommandHandler roleCommandHandler;
    private final RoleQueryHandler roleQueryHandler;

    @GetMapping("/roles")
    @PreAuthorize("hasAuthority(T(io.sein.bindr.account.Permissions).CAN_READ_ROLES)")
    List<Role> listRoles() {
        return roleQueryHandler.handle(new ListRolesQuery());
    }

    @PostMapping("/roles")
    @PreAuthorize("hasAuthority(T(io.sein.bindr.account.Permissions).CAN_WRITE_ROLES)")
    void createRole(@RequestBody RoleDefinitionRequest role) {
        roleCommandHandler.handle(
                new CreateRole(
                        role.getName(),
                        role.getDescription(),
                        role.getPermissions()
                ));
    }

    @GetMapping("/roles/{id}")
    @PreAuthorize("hasAuthority(T(io.sein.bindr.account.Permissions).CAN_READ_ROLES)")
    Optional<Role> getRoleById(@PathVariable Long id) {
        return roleQueryHandler.handle(new GetRoleQuery(id));
    }

    @PutMapping("/roles/{id}")
    @PreAuthorize("hasAuthority(T(io.sein.bindr.account.Permissions).CAN_WRITE_ROLES)")
    void updateRoleById(@PathVariable Long id, @RequestBody RoleDefinitionRequest role) {
        roleCommandHandler.handle(new ModifyRole(id, role.getDescription(), role.getPermissions()));
    }

    @DeleteMapping("/roles/{id}")
    @PreAuthorize("hasAuthority(T(io.sein.bindr.account.Permissions).CAN_WRITE_ROLES)")
    void deleteRoleById(@PathVariable Long id) {
        roleCommandHandler.handle(new RemoveRole(id));
    }
}

package io.sein.bindr.account.factory;

import io.sein.bindr.account.RoleFactory;
import io.sein.bindr.account.commands.CreateRole;
import io.sein.bindr.account.entity.Role;
import io.sein.bindr.account.policy.RolePolicy;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class RoleFactoryImpl implements RoleFactory<CreateRole> {

    private final RolePolicy rolePolicy;

    @Override
    public Role createRole(CreateRole parameters) {
        rolePolicy.isAllowed(parameters);

        return Role.builder()
                .name(parameters.getName())
                .description(parameters.getDescription())
                .permissions(parameters.getPermissions())
                .build();
    }
}

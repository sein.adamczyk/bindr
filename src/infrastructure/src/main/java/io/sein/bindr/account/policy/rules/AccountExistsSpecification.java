package io.sein.bindr.account.policy.rules;

import io.sein.bindr.account.AccountRepository;
import io.sein.bindr.core.DomainSpecification;
import io.sein.bindr.core.exceptions.UnmetSpecificationException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class AccountExistsSpecification implements DomainSpecification<String> {

    private final AccountRepository repository;

    @Override
    public void isSatisfiedBy(String email) {
        if (repository.findByEmail(email).isPresent()) {
            throw new UnmetSpecificationException("Account already exists");
        };
    }
}

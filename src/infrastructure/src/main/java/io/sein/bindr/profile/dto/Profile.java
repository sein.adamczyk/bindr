package io.sein.bindr.profile.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

/**
 * Profile
 */
@Validated



public class Profile   {
  @JsonProperty("id")
  private String id = null;

  @JsonProperty("accountId")
  private String accountId = null;

  @JsonProperty("displayName")
  private String displayName = null;

  @JsonProperty("dateOfBirth")
  private Date dateOfBirth = null;

  /**
   * Gets or Sets gender
   */
  public enum GenderEnum {
    MALE("male"),
    
    FEMALE("female"),
    
    TRANS("trans"),
    
    COUPLE("couple");

    private String value;

    GenderEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static GenderEnum fromValue(String text) {
      for (GenderEnum b : GenderEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("gender")
  private GenderEnum gender = null;

  /**
   * Gets or Sets affinity
   */
  public enum AffinityEnum {
    DOMINANT("dominant"),
    
    SUBMISSIVE("submissive"),
    
    SWITCH("switch");

    private String value;

    AffinityEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static AffinityEnum fromValue(String text) {
      for (AffinityEnum b : AffinityEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("affinity")
  private AffinityEnum affinity = null;

  /**
   * Gets or Sets orientation
   */
  public enum OrientationEnum {
    HETEROSEXUAL("heterosexual"),
    
    HOMOSEXUAL("homosexual"),
    
    BISEXUAL("bisexual"),
    
    OTHER("other");

    private String value;

    OrientationEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static OrientationEnum fromValue(String text) {
      for (OrientationEnum b : OrientationEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("orientation")
  private OrientationEnum orientation = null;

  /**
   * Gets or Sets relationshipPreference
   */
  public enum RelationshipPreferenceEnum {
    MONOGAMOUS("monogamous"),
    
    NON_MONOGAMOUS("non-monogamous"),
    
    UNATTACHED("unattached");

    private String value;

    RelationshipPreferenceEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static RelationshipPreferenceEnum fromValue(String text) {
      for (RelationshipPreferenceEnum b : RelationshipPreferenceEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("relationshipPreference")
  private RelationshipPreferenceEnum relationshipPreference = null;

  /**
   * Gets or Sets relationshipStatus
   */
  public enum RelationshipStatusEnum {
    SINGLE("single"),
    
    DATING("dating"),
    
    MARRIED("married"),
    
    ENGAGED("engaged"),
    
    COMPLICATED("complicated");

    private String value;

    RelationshipStatusEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static RelationshipStatusEnum fromValue(String text) {
      for (RelationshipStatusEnum b : RelationshipStatusEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("relationshipStatus")
  private RelationshipStatusEnum relationshipStatus = null;

  /**
   * Gets or Sets status
   */
  public enum StatusEnum {
    ACTIVE("active"),
    
    INACTIVE("inactive");

    private String value;

    StatusEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static StatusEnum fromValue(String text) {
      for (StatusEnum b : StatusEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("status")
  private StatusEnum status = null;

  @JsonProperty("isSponsored")
  private Boolean isSponsored = false;

  @JsonProperty("pictures")
  @Valid
  private List<Picture> pictures = null;

  @JsonProperty("location")
  private Location location = null;
}


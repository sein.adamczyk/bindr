package io.sein.bindr.account.endpoint;

import io.sein.bindr.account.dto.request.*;
import io.sein.bindr.account.service.AuthenticationService;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1")
public class AccountEndpoint {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    AuthenticationService authenticationService;

    @PostMapping("/accounts/register")
    String registerAccount(@RequestBody @Valid RegisterRequest registerRequest) {
        return authenticationService.register(registerRequest);
    }

    @PostMapping("/accounts/login")
    String loginAccount(@RequestBody LoginRequest loginRequest) {
        return authenticationService.login(loginRequest);
    }

    @GetMapping("/accounts/me")
    @SecurityRequirement(name = "bearerAuth")
    UserDetails aboutMe(@Parameter(hidden = true) @AuthenticationPrincipal UserDetails userDetails) {
        return userDetails;
    }

    @PostMapping("/accounts/deactivate")
    @SecurityRequirement(name = "bearerAuth")
    ResponseEntity<Void> deactivateAccount(@RequestBody PasswordConfirmationRequest passwordConfirmationRequest) {
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    @PostMapping("/accounts/reset-password")
    ResponseEntity<Void> resetAccountPassword(@RequestBody ForgotPasswordRequest forgotPasswordRequest) {
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    @PostMapping("/accounts/reset-password/{resetId}")
    ResponseEntity<Void> resetAccountPassword(@PathVariable String resetId, @RequestBody ResetPasswordRequest resetPasswordRequest) {
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    @PostMapping("/accounts/change-password")
    @SecurityRequirement(name = "bearerAuth")
    ResponseEntity<Void> changeAccountPassword(@RequestBody ChangePasswordRequest changePasswordRequest) {
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }
}

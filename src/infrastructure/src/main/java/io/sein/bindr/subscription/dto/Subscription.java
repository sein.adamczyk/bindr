package io.sein.bindr.subscription.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.validation.annotation.Validated;
import io.sein.bindr.subscription.enums.SubscriptionPeriod;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

/**
 * Subscription
 */
@Validated


@Data
public class Subscription   {
  @JsonProperty("plan")
  private SubscriptionPlan plan = null;

  @JsonProperty("price")
  private BigDecimal price = null;

  @JsonProperty("period")
  private SubscriptionPeriod period = null;

  @JsonProperty("since")
  private OffsetDateTime since = null;

  @JsonProperty("until")
  private OffsetDateTime until = null;

  @JsonProperty("isRecurring")
  private Boolean isRecurring = true;
}


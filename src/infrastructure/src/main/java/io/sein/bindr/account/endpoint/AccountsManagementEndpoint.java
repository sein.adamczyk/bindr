package io.sein.bindr.account.endpoint;

import io.sein.bindr.account.AccountCommandHandler;
import io.sein.bindr.account.AccountQueryHandler;
import io.sein.bindr.account.commands.AssignRole;
import io.sein.bindr.account.commands.DeleteAccount;
import io.sein.bindr.account.commands.RevokeRole;
import io.sein.bindr.account.commands.UpdateAccount;
import io.sein.bindr.account.dto.request.AccountRequest;
import io.sein.bindr.account.dto.response.AccountResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1")
@SecurityRequirement(name = "bearerAuth")
public class AccountsManagementEndpoint {

    private final AccountQueryHandler accountQueryHandler;
    private final AccountCommandHandler accountCommandHandler;

    @GetMapping("/accounts")
    @PreAuthorize("hasAuthority(T(io.sein.bindr.account.Permissions).CAN_READ_USERS)")
    List<AccountResponse> listAccounts() {
        return accountQueryHandler.listAccounts();
    }

    @GetMapping("/accounts/{id}")
    @PreAuthorize("hasAuthority(T(io.sein.bindr.account.Permissions).CAN_READ_USERS)")
    AccountResponse getAccountById(@PathVariable Long id) {
        return accountQueryHandler.getAccount(id);
    }

    @PutMapping("/accounts/{id}")
    @PreAuthorize("hasAuthority(T(io.sein.bindr.account.Permissions).CAN_MANAGE_USERS)")
    void updateAccountById(@PathVariable Long id, @RequestBody AccountRequest account) {
        accountCommandHandler.handle(new UpdateAccount(id, account.getEmail(), account.getPhoneNr()));
    }

    @DeleteMapping("/accounts/{id}")
    void deleteAccountById(@PathVariable Long id) {
        accountCommandHandler.handle(new DeleteAccount(id));
    }

    @PostMapping("/accounts/{id}/role/{roleId}/assign")
    void assignRoleToAccount(@PathVariable Long id, @PathVariable Long roleId) {
        accountCommandHandler.handle(new AssignRole(id, roleId));
    }

    @PostMapping("/accounts/{id}/role/{roleId}/revoke")
    void revokeRoleFromAccount(@PathVariable Long id, @PathVariable Long roleId) {
        accountCommandHandler.handle(new RevokeRole(id, roleId));
    }
}

package io.sein.bindr.core.endpoint;

import io.sein.bindr.core.dto.ErrorResponse;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

@RestController
public class ApplicationErrorController implements ErrorController {

    private static final String PATH = "/error";

    @RequestMapping(value = "/error")
	public ErrorResponse errorPage(HttpServletRequest request) {
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
        String message = request.getAttribute(RequestDispatcher.ERROR_MESSAGE).toString();
        if (status != null) {

            int statusCode = Integer.parseInt(status.toString());
		    return new ErrorResponse(HttpStatus.valueOf(statusCode), message );
        }
		return new ErrorResponse(HttpStatus.NOT_FOUND, "Page not found" );
	}

    public String getErrorPath() {
        return null;
    }
}

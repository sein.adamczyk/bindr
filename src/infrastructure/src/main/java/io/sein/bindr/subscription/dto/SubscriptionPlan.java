package io.sein.bindr.subscription.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.validation.annotation.Validated;
import io.sein.bindr.subscription.enums.SubscriptionPeriod;
import io.sein.bindr.subscription.enums.SubscriptionPlanStatus;

import java.math.BigDecimal;

/**
 * SubscriptionPlan
 */
@Validated


@Data
public class SubscriptionPlan   {
  @JsonProperty("id")
  private String id = null;

  @JsonProperty("title")
  private String title = null;

  @JsonProperty("description")
  private String description = null;

  @JsonProperty("price")
  private BigDecimal price = null;

  @JsonProperty("period")
  private SubscriptionPeriod period = null;

  @JsonProperty("status")
  private SubscriptionPlanStatus status = null;

}


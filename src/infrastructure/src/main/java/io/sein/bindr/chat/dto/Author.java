package io.sein.bindr.chat.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

/**
 * Author
 */
@Validated


@Data
public class Author   {
  @JsonProperty("profileId")
  private String profileId = null;

  @JsonProperty("displayName")
  private String displayName = null;

  @JsonProperty("avatarUrl")
  private String avatarUrl = null;

}


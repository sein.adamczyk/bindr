package io.sein.bindr.subscription.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;

/**
 * Balance
 */
@Validated


@Data
public class Balance   {
  @JsonProperty("accountId")
  private String accountId = null;

  @JsonProperty("totalSum")
  private BigDecimal totalSum = null;

  @JsonProperty("operations")
  @Valid
  private List<Operation> operations = null;

}


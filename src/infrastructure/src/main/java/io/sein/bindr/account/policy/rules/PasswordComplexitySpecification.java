package io.sein.bindr.account.policy.rules;

import io.sein.bindr.core.DomainSpecification;
import io.sein.bindr.core.exceptions.UnmetSpecificationException;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@NoArgsConstructor
public class PasswordComplexitySpecification implements DomainSpecification<String> {

    @Override
    public void isSatisfiedBy(String password) {
        if (password.length() < 8) {
            throw new UnmetSpecificationException("Password is too short");
        }
    }
}

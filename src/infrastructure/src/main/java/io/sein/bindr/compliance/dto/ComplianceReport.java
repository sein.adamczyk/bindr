package io.sein.bindr.compliance.dto;

import io.sein.bindr.compliance.enums.ComplianceStatus;
import io.sein.bindr.compliance.enums.ComplianceType;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

/**
 * ComplianceReport
 */
@Validated



@Data
public class ComplianceReport   {
  @JsonProperty("reportAgainst")
  private String reportAgainst = null;

  /**
   * Gets or Sets type
   */


  @JsonProperty("type")
  private ComplianceType type = null;

  @JsonProperty("content")
  private String content = null;

  @JsonProperty("status")
  private ComplianceStatus status = null;

}


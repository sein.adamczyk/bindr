package io.sein.bindr.account;

import io.sein.bindr.account.dto.response.AccountResponse;
import io.sein.bindr.account.entity.Account;
import io.sein.bindr.core.exceptions.DomainNotFoundException;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class AccountQueryHandler {

    private final AccountRepository accountRepository;
    private final ModelMapper modelMapper = new ModelMapper();

    public List<AccountResponse> listAccounts() {
        return accountRepository.findAll()
                .stream().map(account -> modelMapper.map(account, AccountResponse.class))
                .collect(Collectors.toList());
    }

    public AccountResponse getAccount(Long id) {
        Account account = accountRepository.findById(id)
                .orElseThrow(DomainNotFoundException::new);

        return modelMapper.map(account, AccountResponse.class);
    }
}

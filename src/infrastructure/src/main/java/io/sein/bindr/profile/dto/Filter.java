package io.sein.bindr.profile.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.validation.annotation.Validated;
import io.sein.bindr.profile.enums.GenderEnum;

/**
 * Filter
 */
@Validated
@Data
public class Filter   {
  @JsonProperty("ageFrom")
  private Integer ageFrom = null;

  @JsonProperty("ageTo")
  private Integer ageTo = null;

  @JsonProperty("distance")
  private Integer distance = null;

  @JsonProperty("gender")
  private GenderEnum gender = null;

}


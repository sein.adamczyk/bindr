package io.sein.bindr.account.service;

import io.sein.bindr.account.AccountRepository;
import io.sein.bindr.account.entity.Account;
import io.sein.bindr.account.Permissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Primary
@Service
@Transactional
public class AccountDetailsService implements UserDetailsService {

    @Autowired
    private AccountRepository accountRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        Optional<Account> account = accountRepository.findByEmail(email);

        if (account.isEmpty()) {
            throw new UsernameNotFoundException(
                    "No user found with username: " + email);
        }
        Account user = account.get();

        return org.springframework.security.core.userdetails.User
                .withUsername(user.getEmail())
                .password(user.getPassword())
                .authorities(getAuthorities(user.getAuthorities()))
                .accountExpired(false)
                .accountLocked(false)
                .credentialsExpired(false)
                .disabled(false)
                .build();
    }

    private static List<GrantedAuthority> getAuthorities(List<Permissions> role) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        role.forEach((permission) -> {
            authorities.add(new SimpleGrantedAuthority(permission.toString()));
        });
        return authorities;
    }
}

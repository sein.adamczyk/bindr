package io.sein.bindr.account;

import io.sein.bindr.account.commands.CreateRole;
import io.sein.bindr.account.commands.ModifyRole;
import io.sein.bindr.account.commands.RemoveRole;
import io.sein.bindr.account.entity.Role;
import io.sein.bindr.account.events.RoleCreatedEvent;
import io.sein.bindr.account.events.RoleModifiedEvent;
import io.sein.bindr.account.events.RoleRemovedEvent;
import io.sein.bindr.account.policy.RolePolicy;
import io.sein.bindr.core.events.DomainEventsPublisher;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class RoleCommandHandler {
    private final RoleRepository roleRepository;
    private final DomainEventsPublisher eventsPublisher;
    private final RolePolicy rolePolicy;
    private final RoleFactory<CreateRole> roleFactory;

    public void handle(CreateRole command) {
        rolePolicy.isAllowed(command);

        Role role = roleFactory.createRole(command);
        roleRepository.saveAndFlush(role);
        eventsPublisher.publish(new RoleCreatedEvent(this, role));
    }

    public void handle(ModifyRole command) {
        rolePolicy.isAllowed(command);

        Role role = roleRepository.findById(command.getRoleId())
                .orElseThrow(RuntimeException::new);
        role.setDescription(command.getDescription());
        role.setPermissions(
                command.getPermissions());
        roleRepository.saveAndFlush(role);
        eventsPublisher.publish(new RoleModifiedEvent(this, role));
    }

    public void handle(RemoveRole command) {
        rolePolicy.isAllowed(command);

        roleRepository.deleteById(command.getRoleId());
        eventsPublisher.publish(new RoleRemovedEvent(this, command.getRoleId()));
    }
}

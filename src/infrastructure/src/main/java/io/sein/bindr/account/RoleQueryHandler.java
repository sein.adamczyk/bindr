package io.sein.bindr.account;

import io.sein.bindr.account.entity.Role;
import io.sein.bindr.account.queries.GetRoleQuery;
import io.sein.bindr.account.queries.ListRolesQuery;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class RoleQueryHandler {
    private final RoleRepository roleRepository;

    public List<Role> handle(ListRolesQuery query) {
        return this.roleRepository.findAll();
    }

    public Optional<Role> handle(GetRoleQuery query) {
        return this.roleRepository.findById(query.getRoleId());
    }
}

package io.sein.bindr.account.service;

import io.sein.bindr.account.AccountFactory;
import io.sein.bindr.account.AccountRepository;
import io.sein.bindr.account.auth.JwtTokenProvider;
import io.sein.bindr.account.commands.RegisterAccount;
import io.sein.bindr.account.dto.request.LoginRequest;
import io.sein.bindr.account.dto.request.RegisterRequest;
import io.sein.bindr.account.entity.Account;
import io.sein.bindr.core.exceptions.CustomException;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthenticationServiceImpl implements AuthenticationService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final PasswordEncoder passwordEncoder;
    private final AccountRepository repository;
    private final AccountFactory<RegisterAccount> accountFactory;
    private final JwtTokenProvider jwtTokenProvider;

    @Override
    public String register(RegisterRequest request) {
        Account account = accountFactory
                .createAccount(new RegisterAccount(request.getEmail(), request.getPassword()));
        repository.save(account);

        return jwtTokenProvider.createToken(account.getEmail(), account.getAuthorities());
    }

    @Override
    public String login(LoginRequest request) {
        Account account = repository.findByEmail(request.getEmail())
                .orElseThrow(() -> new CustomException("Invalid username/password supplied", HttpStatus.BAD_REQUEST));
        if (!passwordEncoder.matches(request.getPassword(), account.getPassword())) {
            throw new CustomException("Invalid username/password supplied", HttpStatus.BAD_REQUEST);
        }
        logger.info("Logging in User {} with permissions... {}", account.getEmail(), account.getAuthorities());
        return jwtTokenProvider.createToken(account.getEmail(), account.getAuthorities());
    }

}

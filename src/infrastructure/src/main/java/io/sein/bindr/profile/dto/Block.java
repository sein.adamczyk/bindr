package io.sein.bindr.profile.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

import java.time.OffsetDateTime;

/**
 * Block
 */
@Validated


@Data
public class Block   {
  @JsonProperty("io/sein/bindr/profile")
  private Profile profile = null;

  @JsonProperty("createdAt")
  private OffsetDateTime createdAt = null;

}


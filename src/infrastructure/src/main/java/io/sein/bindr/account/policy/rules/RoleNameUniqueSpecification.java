package io.sein.bindr.account.policy.rules;

import io.sein.bindr.account.RoleRepository;
import io.sein.bindr.core.DomainSpecification;
import io.sein.bindr.core.exceptions.UnmetSpecificationException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class RoleNameUniqueSpecification implements DomainSpecification<String> {

    private final RoleRepository repository;

    @Override
    public void isSatisfiedBy(String roleName) {
        if (!repository.findAllByName(roleName).isEmpty())
        {
            throw new UnmetSpecificationException("Role does not exist");
        }
    }

    public void isSatisfiedBy(Long id) {
        if (!repository.findById(id).isEmpty()) {
            throw new UnmetSpecificationException("Role does not exist");
        }
    }
}

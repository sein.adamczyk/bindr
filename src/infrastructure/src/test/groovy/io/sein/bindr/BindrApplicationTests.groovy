package io.sein.bindr

import io.sein.bindr.account.endpoint.AccountEndpoint
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest
class BindrApplicationTests extends Specification {

    @Autowired(required = false)
    private AccountEndpoint endpoint

    def "when context is loaded then all expected beans are created"() {
        expect:
        "the WebController is created"
        endpoint
    }
}

package io.sein.bindr.account.events;

import io.sein.bindr.account.entity.Role;
import io.sein.bindr.core.events.BaseDomainEvent;
import lombok.Getter;

public class RoleCreatedEvent extends BaseDomainEvent {

    @Getter
    private final Role role;

    public RoleCreatedEvent(Object source, Role role) {
        super(source);
        this.role = role;
    }

    @Override
    public String type() {
        return RoleCreatedEvent.class.getName();
    }
}

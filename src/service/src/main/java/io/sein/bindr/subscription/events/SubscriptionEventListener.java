package io.sein.bindr.subscription.events;

import io.sein.bindr.account.events.RoleCreatedEvent;
import io.sein.bindr.account.events.RoleModifiedEvent;
import io.sein.bindr.account.events.RoleRemovedEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class SubscriptionEventListener {

    @EventListener
    public void listenTo(RoleCreatedEvent event) {
        log.info("Handling {} of {}", event.getClass().getName() , event.getRole().getName());
    }

    @EventListener
    public void listenTo(RoleModifiedEvent event) {
        log.info("Handling {} of {}", event.getClass().getName(), event.getRole().getName());
    }

    @EventListener
    public void listenTo(RoleRemovedEvent event) {
        log.info("Handling {} of {}", event.getClass().getName(), event.getRoleId());
    }
}

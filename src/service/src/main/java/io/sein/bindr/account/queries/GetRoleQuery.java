package io.sein.bindr.account.queries;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class GetRoleQuery {
    private final Long roleId;
}

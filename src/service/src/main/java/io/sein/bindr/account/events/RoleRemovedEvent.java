package io.sein.bindr.account.events;

import io.sein.bindr.core.events.BaseDomainEvent;
import lombok.Getter;

public class RoleRemovedEvent extends BaseDomainEvent {

    @Getter
    private final Long roleId;

    public RoleRemovedEvent(Object source, Long roleId) {
        super(source);
        this.roleId = roleId;
    }

    @Override
    public String type() {
        return RoleRemovedEvent.class.getName();
    }
}

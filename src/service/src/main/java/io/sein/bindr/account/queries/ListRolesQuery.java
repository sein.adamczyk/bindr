package io.sein.bindr.account.queries;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class ListRolesQuery {
}
